﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using WSVols.Model;

namespace WSVols
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class Vols : 
        System.Web.Services.WebService
    {

        [WebMethod]
        public List<Vol> Horaire()
        {
            return Vol.GetHoraire();
        }
        [WebMethod]
        public List<string> Departs()
        {
            return Vol.GetDeparts();
        }
    }
}
