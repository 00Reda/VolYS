﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WSVols.Model
{
    public class Vol
    {
        public int Num { get; set; }
        public string Depart { get; set; }
        public string Arrivee { get; set; }
        public string Heure { get; set; }

        public Vol()
        {

        }

        public Vol(int num, string depart, string arrivee, string heure)
        {
            Num = num;
            Depart = depart;
            Arrivee = arrivee;
            Heure = heure;
        }
        public Vol(int num, string depart, string arrivee, int hr, int min)
        {
            Num = num;
            Depart = depart;
            Arrivee = arrivee;
            Heure = $"{hr}:{min}";
        }

        public static List<string> GetDeparts()
        {
            var vols = GetHoraire();
            return vols.Select(x => x.Depart).Distinct().ToList();
        }

        public static List<Vol> GetHoraire()
        {
            List<Vol> vols = new List<Vol>();
            vols.Add(new Vol(1, "Casa","Tanger",7,30));
            vols.Add(new Vol(2, "Tanger", "Paris", 8, 40));
            vols.Add(new Vol(3, "Tanger", "Madrid", 13, 10));
            vols.Add(new Vol(4, "Tanger", "Agadir", 14, 15));
            vols.Add(new Vol(5, "Paris", "Tanger", 16, 30));

            return vols;
        }

    }
}