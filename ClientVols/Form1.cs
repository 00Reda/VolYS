﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientVols
{
    public partial class Form1 : Form
    {
        srv.Vols svol = new srv.Vols();
        public Form1()
        {
            InitializeComponent();
        }

        private void btnAfficher_Click(object sender, EventArgs e)
        {
            var res = svol.Departs().Select(d => new Ville(d)).ToList();
           
            grd.DataSource = res;
        }

      
    }
}
