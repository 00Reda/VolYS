﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientVols
{
    class Ville
    {
        public string Nom { get; set; }
        public Ville()
        {

        }

        public Ville(string nom)
        {
            Nom = nom;
        }
    }
}
